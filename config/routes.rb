MwhJi::Application.routes.draw do
  get 'answer/:id' => "questions#answer", :as => "answer_question"
  post 'answer/:id' => "questions#post_answer", :as => "post_answer"
  get "login" => "game#login", :as => "login"
  get "logout" => "sessions#destroy", :as => "logout"
  root :to => 'game#index'
  resources :sessions, :users, :questions, :answers
end
