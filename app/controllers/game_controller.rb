class GameController < ApplicationController
	skip_before_filter :check_session, :only=> [:login]

	def index
		@users_by_points = User.all.sort! { |a,b| a.points <=> b.points }.reverse()
	end

	def login
	end
end
