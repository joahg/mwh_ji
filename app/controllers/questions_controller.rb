class QuestionsController < ApplicationController
	def new
		@question = Question.new
	end

	def create
		@question = Question.new(params[:question])
		if @question.save
			redirect_to root_url
		else
			render "new"
		end
	end

	def answer
		@question = Question.find(params[:id])
	end
end
