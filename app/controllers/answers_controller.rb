class AnswersController < ApplicationController

	def create
		@answer = Answer.new(params[:answer])
		@question = Question.find(@answer.question_id)

		if @answer.content == @question.answer
			@answer.point_value = @question.point_value
		else
			@answer.point_value = 0 - @question.point_value
		end

		@content = @answer.content

		if @answer.save
			respond_to do |format|
				format.js
			end
		end
	end

end
