class Answer < ActiveRecord::Base
  attr_accessible :content, :question_id, :user_id, :point_value

  belongs_to :user
  belongs_to :post
end
