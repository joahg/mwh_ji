class User < ActiveRecord::Base
  has_secure_password

  attr_accessible :email, :login, :password, :password_confirmation, :admin, :name

  has_many :questions
  has_many :answers

  def points
  	t = 0
  	self.answers.all.each do |a|
  		t += a.point_value
  	end
  	t
  end
end
