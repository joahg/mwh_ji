class Question < ActiveRecord::Base
  attr_accessible :answer, :query, :picture, :user_id, :point_value

  has_attached_file :picture

  belongs_to :user
  has_many :answers

  def preview
  	if self.query.length > 15
  		self.query[0..11]+"..."
  	else
  		self.query
  	end
  end
end
