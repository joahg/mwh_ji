class AddPointValueToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :point_value, :integer
  end
end
