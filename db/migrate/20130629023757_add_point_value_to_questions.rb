class AddPointValueToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :point_value, :integer, :default => 5
  end
end
